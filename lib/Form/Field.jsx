import React from 'react';
import classNames from 'classnames';
import { Form } from 'react-deepgarden';

import { Input } from '../../lib';

export default function Field({ isAccent, className, component, type, label, legend, ...props }) {
	const Component = type ? Input[type] : component;
	return (
		<Form.Field
			input={Component}
			{...props}
			className={classNames({ '_Form__Field--isAccent': isAccent }, className)}
			label={label && (<>
				{label}
				{legend && <span> {legend}</span>}
			</>)}
		/>
	);
}
