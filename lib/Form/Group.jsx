import React from 'react';

export default function FormGroup({ title, legend, children, ...props }) {
	return <div className="_Form__Group" {...props}>
		{title && (
			<h2>
				{title}
			</h2>
		)}
		{legend && (
			<legend>
				{legend}
			</legend>
		)}
		{children}
	</div>
}
