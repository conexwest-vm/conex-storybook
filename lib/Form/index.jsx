import React from 'react';

import { Form } from 'react-deepgarden';

import Group from './Group';
import Field from './Field';
import Control from './Control';

export default {
	Init: Form,
	Form: Form.Form,
	Group: Group,
	Field: Field,
	Control: Control,
};

import './index.styl';
