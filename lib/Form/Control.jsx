import React from 'react';

export default function Control({ children }) {
	return (
		<div className="_Form__Control">
			{children}
		</div>
	);
}
