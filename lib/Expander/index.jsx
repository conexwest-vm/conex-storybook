import React from 'react';

import { Icon } from '../index';

export default function Expander({ isExpanded, toggleExpand }) {
	const Chevron = isExpanded ? Icon.ChevronUp : Icon.ChevronDown;
	return (
		<div className="_Expander">
			<Chevron onClick={toggleExpand} />
		</div>
	);
}

import './index.styl';
