import React from 'react';
import T from 'prop-types';
import { Button as Component } from 'react-deepgarden';

import './index.styl';

export default function Button({
	pending,
	children,
	icon,
	iconPudding,
	...props
}) {
	return (
		<Component {...props}>
			{icon && (
				<div style={{ paddingRight: iconPudding }} className="_Button__Icon">
					{icon}
				</div>
			)}
			{children}
		</Component>
	);
}

Button.propTypes = {
	children: T.node.isRequired,
	icon: T.node,
	iconPudding: T.number,
};

Button.defaultProps = {
	icon: null,
	iconPudding: 6,
};
