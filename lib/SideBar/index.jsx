import React from 'react';
import T from 'prop-types';
import { Link, NavLink } from 'react-router-dom';

import { Icon } from '../index';

const SideBar = ({ items, onLogout }) => {
	return (
		<div className="_SideBar">
			<div>
				<Link to="/" className="_SideBar__Back">
					<Icon.ArrowLeft className="_SideBar__Icon"/>
					Settings
				</Link>
				{items.map((item, key) => {
					const IconComponent = Icon[item.icon];
					return (
						<NavLink
							to={item.path}
							className="_SideBar__Item"
							key={key}
						>
							<IconComponent className="_SideBar__Icon"/>
							{item.label}
						</NavLink>
					);
				})}
			</div>
			<div className="_SideBar__LogOut" onClick={onLogout}>
				<Icon.LogOut className="_SideBar__Icon"/>
				Log Out
			</div>
		</div>
	);
};

SideBar.propTypes = {
	items: T.array,
	onLogout: T.func.isRequired,
};

SideBar.defaultProps = {
	items: []
};

export default SideBar;

import './index.styl';
