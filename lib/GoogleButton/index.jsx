import React from 'react';

import { GoogleLogo } from '../Icon';

const GoogleButton = (props) => (
  <button {...props} className="_GoogleButton">
    <GoogleLogo />
    Continue with Google
  </button>
);

export default GoogleButton;

import './index.styl';
