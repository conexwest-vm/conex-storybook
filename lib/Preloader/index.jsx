import React from 'react';

export default function Preloader({ distinct = false }) { //@TODO: Refactor
    return distinct && (
        <div className="_Loader">
            <div className="_Loader__Spinner">
                <div className="_Preloader__Wrapper">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    ) || (
        <div className="_Loader">
            <div className="_Loader__Spinner loadingio-spinner-dual-ball-3ddxoao3mxm">
                <div className="ldio-sqh4xjnahxf">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    )
}

import './index.styl';
