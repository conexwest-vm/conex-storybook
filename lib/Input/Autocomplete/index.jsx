import React, { useState } from 'react';

import DropDown from '../../DropDown';
import Menu from '../../Menu';

import input from '../input';

function InputAutocomplete({
	className,
	value,
	onChange,
	autocomplete = () => [],
	...props
}) {
	const [search, setSearch] = useState(value ? value.toString() : '');
	const [options, setOptions] = useState([]);
	return (
		<div className={className}>
			<input
				{...props}
				value={search}
				onChange={async (e) => {
					setSearch(e.target.value);
					const result = await autocomplete(e.target.value);
					setOptions(result);
				}}
				autoComplete="off"
			/>
			{!!options.length && (
				<div className="_InputAutocomplete__DropDown">
					<Menu
						options={options}
						onSelect={(option) => {
							setSearch(option.label);
							setOptions([]);
							onChange(option);
						}}
					/>
				</div>
			)}
		</div>
	);
}

export default input('_InputAutocomplete')(InputAutocomplete);

import './index.styl';
