import React from 'react';

import input from '../input';

function InputNumber({
	className,
	value,
	onChange,
	...props
}) {
	return (
		<div className={className}>
			<input
				{...props}
				value={value ? value.toString() : ''}
				onChange={(e) => {
					onChange(+e.target.value);
				}}
			/>
		</div>
	);
}

export default input('_InputNumber')(InputNumber);

import './index.styl';
