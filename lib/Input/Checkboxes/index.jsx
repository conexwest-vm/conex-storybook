import React from 'react';

import input from '../input';

export default
@input('_InputCheckboxes', {
	wrapper: 'span'
})
class InputCheckboxes extends React.Component {
	handleChange = (e) => {
		const value = e.target.checked
			? [...this.props.value, e.target.value]
			: this.props.value.filter(option => option !== e.target.value)
		this.props.onChange(value);
	}
	render() {
		return <div className={this.props.className}>
			{Object.keys(this.props.options).map(key => (
				<label className="_InputCheckboxes__Option" key={key}>
					<input type="checkbox" checked={this.props.value.includes(key)} onChange={this.handleChange} value={key} />
					<span>
						{this.props.options[key]}
					</span>
				</label>
			))}
		</div>
	}
}

import './index.styl';
