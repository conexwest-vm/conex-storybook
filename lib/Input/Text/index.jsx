import React from 'react';
import T from 'prop-types';

import input from '../input';

function InputText({
	className,
	value,
	...props
}) {
	return (
		<div className={className}>
			<input {...props} value={value ? value.toString() : ''} />
		</div>
	);
}

export default input('_InputText')(InputText);

import './index.styl';
