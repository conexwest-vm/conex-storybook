import React, { useState } from 'react';
import classNames from 'classnames';
import withClassName from 'react-deepgarden/hoc/withClassName';

export default function input(inputClassName, { wrapper = 'label' } = {}) {
	return (Component) => withClassName(inputClassName)(class extends React.Component {
		state = {
			isFocused: false,
		}
		setFocused = (isFocused) => {
			this.setState({
				isFocused,
			})
		}
		render() {
			const { className, onFocus, onBlur, label, ...props } = this.props;
			const Wrapper = wrapper;
			return (
				<Wrapper
					className={classNames(
						className,
						'_Input',
						{ '_Input--isFocused': this.state.isFocused },
					)}
					onFocus={(e) => {
						this.setFocused(true);
						onFocus && onFocus(e);
					}}
					onBlur={(e) => {
						this.setFocused(false);
						onBlur && onBlur(e);
					}}
				>
					{!!label && (
						<div className="_Input__Label">
							{label}
						</div>
					)}
					<Component
						{...props}
						className="_Input__Input"
					/>
				</Wrapper>
			);
		}
	})
}

import './index.styl';
