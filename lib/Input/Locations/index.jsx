import React from 'react';

import input from '../input';

export default
@input('_InputCheckboxes', {
	wrapper: 'span'
})
class InputCheckboxes extends React.Component {
	handleChange = (e) => {
		this.props.onChange(e.target.checked
			? [...this.props.value, e.target.value]
			: this.props.value.filter(location => location._id !== e.target.value));
	}
	render() {
		return <div className={this.props.className}>
			{this.props.locations.map((location, key) => (
				<label className="_InputCheckboxes__Option" key={key}>
					<input
						type="checkbox"
						checked={!!this.props.value?.includes(location._id)}
						onChange={this.handleChange}
						value={location._id} />
					<span>
						{location.name}
					</span>
				</label>
			))}
		</div>
	}
}

import './index.styl';
