import React from 'react';
import T from 'prop-types';
import ReactSelect, { components } from 'react-select';

import { AngleDown, Search } from '../../Icon';

import input from '../input';

function DropdownIndicator({ isFocused }) {
	return (
		<div className="_InputSelect__Indicator">
			{isFocused ? (
				<Search />
			) : (
				<AngleDown />
			)}
		</div>
	)
}

export default
@input('_InputSelect')
class InputSelect extends React.Component {
	handleChange = (option) => {
		this.props.onChange(option.value);
	}
	render() {
		const {
			className,
			disabled,
			value,
			options = [],
			...props
		} = this.props;
		const selected = options.find(option => option.value === value);
		return (
			<ReactSelect
				{...props}
				classNamePrefix="_InputSelect"
				className={className}
				isDisabled={disabled}
				components={{ DropdownIndicator }}
				options={options}
				value={selected}
				onChange={this.handleChange}
			/>
		)
	}
}

import './index.styl';
