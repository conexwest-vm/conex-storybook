import React from 'react';

import Button from '../../Button';

import input from '../input';

export default
@input('_InputAvatar')
class InputAvatar extends React.Component {
	render() {
		return <div className={this.props.className}>
			<img src={this.props.value} className="_InputAvatar__Avatar" />
			<Button className="--isBlock">Upload new photo</Button>
		</div>
	}
}

import './index.styl';
