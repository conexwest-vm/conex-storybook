import React from 'react';
import classNames from 'classnames';

import Button from '../../Button';
import Preloader from '../../Preloader';

import input from '../input';

export default
@input('_InputImage')
class InputAvatar extends React.Component {
	inputRef = React.createRef();
	state = {
		pending: false,
	}
	handleUpload = async (e) => {
		try {
			this.setState({
				pending: true,
			});
			const [file] = e.target.files;
			const imageUrl = await this.props.upload(file);
			if (!imageUrl) {
				return;
			}
			this.props.onChange(imageUrl);
		} finally {
			this.setState({
				pending: false,
			});
		}
	}
	handleUploadClick = () => {
		this.inputRef.current.click();
	}
	render() { //@TODO: Preloader
		return <div className={this.props.className}>
			<div
				className={classNames('_InputImage__Image', { '_InputImage__Image--Placeholder': !this.props.value })}
				style={this.state.pending ? {} : { backgroundImage: `url(${this.props.value})` }}
			>
				{this.state.pending && (
					'Loading...'
				)}
			</div>
			<input type="file" onChange={this.handleUpload} ref={this.inputRef} />
			<Button className="--isBlock" onClick={this.handleUploadClick}>{this.props.button}</Button>
		</div>
	}
}

import './index.styl';
