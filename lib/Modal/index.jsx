import React from 'react';
import {Modal} from 'react-deepgarden';
import classNames from 'classnames';
import Preloader from '../Preloader';

import { Close } from '../Icon';

export default ({ pending = false, children, title, width = 'auto', onClose, ...props }) => (
	<Modal className={classNames('_Modal--isPending')} {...props}>
		{pending ? <Preloader /> : (
			<div className="_Modal__Modal" style={{ width }}>
				<div className="_Modal__Header">
					<div className="_Modal__Title">
						{title}
					</div>
					<div className="_Modal__Closing" onClick={onClose}>
						<Close />
					</div>
				</div>
				<div className="_Modal__Body">
					{children}
				</div>
			</div>
		)}
	</Modal>
);

import './index.styl';
