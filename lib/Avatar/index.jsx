import React from 'react';
import T from 'prop-types';
import classNames from 'classnames';
import { withClassName } from 'react-deepgarden';

function Avatar({ src, name, className }) {
  return (
      <div className={className}>
        {src ? (
            <img src={src} />
        ) : (
            <span>{name[0]}</span>
        )}
      </div>
  )
}

/*const Avatar = (props) => <img {...props} />;*/

Avatar.propTypes = {
  src: T.string,
  className: T.string,
  alt: T.string,
};

Avatar.defaultProps = {
  alt: 'alt',
};

import './index.styl';

export default withClassName('_Avatar')(Avatar);
