import React from 'react';

import Avatar from '../Avatar';

export default function Profile({ name }) {
	return (
		<div className="_Profile">
			<div className="_Profile__Name">
				{name}
			</div>
			<Avatar className="_Profile__Avatar" name={name} />
		</div>
	)
}

import './index.styl';
