import React from 'react';
import T from 'prop-types';
import classNames from 'classnames';

import './index.styl';

const TextButton = ({ children, onClick, icon, className }) => {
  const Icon = icon;
  return (
    <span onClick={onClick} className={classNames('_Tex-Button', className)}>
      {icon ? <Icon /> : '+'} <span>{children}</span>
    </span>
  );
};

TextButton.propTypes = {
  children: T.node.isRequired,
  onClick: T.func.isRequired,
  icon: T.node,
  className: T.string,
};

export default TextButton;
