import React from 'react';
import T from 'prop-types';
import './index.styl';
import { Icon } from '../index';

const SearchInput = (props) => (
  <div className="_SearchInput">
    <div className="_SearchInput__Icon">
      <Icon.Search />
    </div>
    <input {...props} onChange={e => props.onChange(e.target.value)} className="_SearchInput__Input" />
  </div>
);

SearchInput.propTypes = {
  placeholder: T.string,
};

SearchInput.defaultProps = {
  placeholder: 'Search',
};

export default SearchInput;
