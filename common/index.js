import T from 'prop-types';

export const TYPES = {
  _types: T.onOf(['success', 'notice', 'danger', 'warning']),
  _size: T.oneOf(['smallest', 'smaller', 'bigger', 'biggest']),
};
