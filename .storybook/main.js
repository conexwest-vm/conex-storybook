const path = require("path");
const deepgarden = require("deepgarden");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  stories: [
    "../stories/**/*.stories.mdx",
    "../stories/**/*.stories.@(js|jsx|ts|tsx)",
  ],
  addons: ["@storybook/addon-links", "@storybook/addon-essentials"],
  webpackFinal: (config) => {
    config.resolve.modules = [
      path.resolve(__dirname, ".."),
      path.resolve(__dirname, "../lib"),
      "node_modules",
    ];
    config.module.rules.push({
      test: /\.styl$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader,
          options: {
            hmr: true,
          },
        },
        "css-loader",
        {
          loader: "stylus-loader",
          options: {
            use: [deepgarden()],
            import: "config.styl",
            include: path.resolve(__dirname, ".."),
          },
        },
      ],
    });
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });
    config.plugins.push(
      new MiniCssExtractPlugin({
        filename: "index.[hash].css",
      })
    );
    return config;
  },
};
