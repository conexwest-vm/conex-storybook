import React, { useState } from 'react';

import Component from 'Modal';
import Button from 'Button';

export default {
  title: 'Modal',
  component: Component,
};

const Template = (args) => {
  const [isOpenModal, setIsOpenModal] = useState(false);
  const closeModal = () => setIsOpenModal(false);
  const openModal = () => setIsOpenModal(true);

  return (
    <>
      <Button onClick={openModal}>Open</Button>
      {isOpenModal && (
        <Component
          closeOnClickOutside={closeModal}
          onClose={closeModal}
          {...args}
        >
          lalalala
        </Component>
      )}
    </>
  );
};

export const Modal = Template.bind({});
Modal.args = {
  pending: true,
};
