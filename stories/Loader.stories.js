import React from 'react';
import Component from 'Loader';

export default {
  title: 'Loader',
  component: Component,
};

const Template = (args) => <Component {...args} />;

export const Loader = Template.bind({});
Loader.args = {
  loading: true,
};
