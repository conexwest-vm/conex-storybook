import React from 'react';

import { Button } from 'lib';

export default {
  title: 'TextButton',
  component: Button,
};

const Template = (args) => <Button {...args}>{args.children}</Button>;

export const TextButton = Template.bind({});
TextButton.args = {
  _type: '',
  children: 'Add something',
  onClick: () => console.log('Click'),
};
