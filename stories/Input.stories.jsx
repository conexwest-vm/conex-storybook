import React from 'react';

import { Input } from 'lib';

export default {
	title: 'Input',
	component: Input,
};

const TextInputTemplate = (args) => (
	<Input.Text {...args} />
);

/*const SelectTemplate = (args) => (
	<Component {...args} />
);*/

export const TextInput = TextInputTemplate.bind({});
TextInput.args = {
	disabled: false,
	placeholder: 'placeholder',
	name: 'input',
	label: 'Input',
};

/*export const Select = SelectTemplate.bind({});
Select.args = {
	disabled: false,
	placeholder: 'Select',
	name: 'select',
	label: 'Select',
	type: 'select',
	options: [
		{ value: 1, label: 'First' },
		{ value: 2, label: 'Secound' },
		{ value: 3, label: 'Third' },
	],
};*/
