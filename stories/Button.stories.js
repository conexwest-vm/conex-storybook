import React from 'react';

import Button from 'Button';
import GoogleButton from 'GoogleButton';
// import TextButton from 'TextButton';
import { Icon } from '../index';

export default {
  title: 'Button',
  component: Button,
};

const Template = (args) => <Button {...args}>{args.children}</Button>;
const GoogleButtonTemplate = (args) => <GoogleButton {...args} />;
// const TextButtonTemplate = (args) => <TextButton {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  _type: '',
  children: 'Button',
};

export const Block = Template.bind({});
Block.args = {
  _block: true,
  children: 'Block Button',
};

export const Success = Template.bind({});
Success.args = {
  _type: 'success',
  children: 'Success',
};

export const Ghost = Template.bind({});
Ghost.args = {
  _type: '',
  _ghost: true,
  children: 'Ghost',
};

export const Size = Template.bind({});
Size.args = {
  _type: 'success',
  _size: 'biggest',
  children: 'Size',
};

export const IconButton = Template.bind({});
IconButton.args = {
  _type: 'success',
  children: 'Save',
  icon: <Icon.Search />,
};

export const Google = GoogleButtonTemplate.bind({});
Google.args = {
  onClick: () => {},
};

// export const Text = TextButtonTemplate.bind({});
// Text.args = {
//   children: 'lalal',
// };
