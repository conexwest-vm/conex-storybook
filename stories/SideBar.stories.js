import React, { useState } from 'react';

import Component from 'SideBar';

export default {
  title: 'SideBar',
  component: Component,
};

const Template = (args) => {
  return (
    <div style={{ display: 'flex', height: '100%' }}>
      <Component
        {...args}
      />
    </div>
  );
};

export const SideBar = Template.bind({});
SideBar.args = {
  items: [
    {label: 'Profile', path: '/settings/profile', icon: 'Profile'},
    {label: 'Company', path: '/settings/company', icon: 'Company'},
    {label: 'Admin', path: '/settings/admin', icon: 'Admin'},
  ]
};
