import React from 'react';

import Tabs from 'Tabs';

export default {
  title: 'Tabs',
  component: Tabs,
};

const Template = (args) => (
  <Tabs>
    {args.tabs.map((item) => (
      <Tabs.Tab title={item.title} key={item.id}>
        {item.children}
      </Tabs.Tab>
    ))}
  </Tabs>
);

export const Primary = Template.bind({});
Primary.args = {
  tabs: [
    { title: 'Company Information', id: 1, children: 'lalala1' },
    { title: 'Locations', id: 2, children: 'lalala2' },
    { title: 'Administrations', id: 3, children: 'lalala3' },
  ],
  onChange: (val) => console.log(val),
};
