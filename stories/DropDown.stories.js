import React from 'react';

import DropDown from 'DropDown';

export default {
  title: 'DropDown',
  component: DropDown,
};

const Template = (args) => <DropDown {...args}>lalalalla</DropDown>;

export const DropDownS = Template.bind({});
// DropDowns.args = {
//   // _type: 'success',
//   // children: 'Success',
// };
