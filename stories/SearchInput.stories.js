import React, { useState } from 'react';

import Component from 'SearchInput';

export default {
  title: 'SearchInput',
  component: Component,
};

const Template = (args) => {
  return <Component {...args} />;
};

export const SearchInput = Template.bind({});
SearchInput.args = {
  placeholder: '',
};
