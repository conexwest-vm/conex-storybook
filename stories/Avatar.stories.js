import React from 'react';

import Component from 'Avatar';

import Logo from './assets/logo.jpeg';

export default {
  title: 'Avatar',
  component: Component,
};

const Template = (args) => <Component {...args} />;

export const Avatar = Template.bind({});

Avatar.args = {
  src: Logo,
  _size: 'smallest',
};
